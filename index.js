require('dotenv').config();
const express = require('express');
const cors = require('cors');
const connection = require('./conf/db.js');
const app = express();
const bcrypt = require('bcrypt');


//Configuration de l'application pour parser les requêtes au format JSON et encodées en URL
app.use(express.json());
app.use(cors({
    origin: 'http://localhost:8080'
})
);
app.use(
    express.urlencoded({
        extended: true,
    })
);


//Ajout du middleware et du controller

// Import du middleware logger
const { logRequest } = require('./logger.middleware');
app.use(logRequest);


// Import et utilisation du controller pour gérer les routes
const garageController = require('./garage.controller.js');
app.use('/garage', garageController);

const port = 3000;


// Établissement de la connexion
connection.getConnection((err) => {
    if (err instanceof Error) {
        console.log('getConnection error:', err);
        return;
    }
});

/* Routes du CRUD: GET, POST, PUT, DELETE

// Route GET pour récupérer toutes les voitures
app.get('/cars', (req, res) => {
    connection.query('SELECT * FROM car', (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la récupération des voitures');
        } else {
            // Envoi du résultat sous forme de JSON si la requête est réussie
            res.json(results);
        }
    });
});

// Route GET pour récupérer une voiture spécifique par son ID en utilisant une requête préparée pour éviter les injections SQL
app.get('/cars/:id', (req, res) => {
    const id = req.params.id;
    // Utilisation d'une requête préparée avec un paramètre pour sécuriser l'accès aux données
    connection.execute('SELECT * FROM car WHERE car_id = ?', [id], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la récupération de la voiture');
        } else {
            res.json(results);
        }
    });
});

// Route POST pour ajouter une nouvelle voiture dans la base de données
app.post('/cars', (req, res) => {
    const { brand, model } = req.body;
    // Exécution d'une requête SQL pour insérer une nouvelle voiture
    connection.execute('INSERT INTO car (brand, model) VALUES (?, ?)', [brand, model], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de l\'ajout de la voiture');
        } else {
            // Confirmation de l'ajout de la voiture avec l'ID de la voiture inséré
            res.status(201).send(`Voiture ajoutée avec l'ID ${results.insertId}`);
        }
    });
});

// Route PUT pour mettre à jour une voiture existante par son identifiant
app.put('/cars/:id', (req, res) => {
    const { brand, model } = req.body;
    const id = req.params.id;
    // Exécution d'une requête SQL pour mettre à jour les informations de la voiture choisie
    connection.execute('UPDATE car SET brand = ?, model = ? WHERE car_id = ?', [brand, model, id], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la mise à jour de la voiture');
        } else {
            // Confirmation de la mise à jour de la voiture
            res.send(`Voiture mise à jour.`);
        }
    });
});

// Route DELETE pour supprimer une voiture par son ID
app.delete('/cars/:id', (req, res) => {
    const id = req.params.id;
    // Exécution d'une requête SQL pour supprimer la voiture choisie
    connection.execute('DELETE FROM car WHERE car_id = ?', [id], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la suppression de la voiture');
        } else {
            // Confirmation de la suppression de la voiture
            res.send(`Voiture supprimée.`);
        }
    });
});

// Route GET pour trouver les voitures d'une marque par son nom
app.get('/cars/brand/:brandName', (req, res) => {
    const brandName = req.params.brandName;

    // Exécution d'une requête SQL pour récupérer les voitures de la marque spécifiée
    connection.execute('SELECT * FROM car WHERE brand = ?', [brandName], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la recherche des voitures de la marque');
        } else {
            if (results.length > 0) {
                // Renvoie la liste des voitures trouvées
                res.json(results);
            } else {
                // Aucune voiture trouvée pour la marque spécifiée
                res.status(404).send('Aucune voiture trouvée pour la marque spécifiée');
            }
        }
    });
});
*/

// Démarrage du serveur Express et écoute sur le port spécifié
app.listen(port, () => {
    console.log(`Serveur en écoute sur le port ${port}`);
});
app.listen(8080, function () {
    console.log('CORS-enabled web server listening on port 80')
})