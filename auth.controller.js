app.post('/register', (req, res) => {
    const { email, password } = req.body;
    if (!email || !password) {
      res
        .status(400)
        .json({ error: 'Please specify both email and password' });
    } else {
// hachage le mot de passe 
bcrypt.hash(password, 10, function(bcryptError, hashedPassword) {
    if (bcryptError) {
      res
        .status(500)
        .json({ error: bcryptError });
    } else {
// enregistrement du compte en base de données
connection.query(
    `INSERT INTO user(email, password) VALUES (?, ?)`,
    [email, hashedPassword],
    (mysqlError, result) => {
      if (mysqlError) {
        res.status(500).json({ error: mysqlError });
      } else {
        // on retourne le compte créé en omettant le mot de passe pour des raisons de sécurité
        res.status(201).json({
          id: result.insertId,
          email,
        });
      }
    }
  );
      }
});
    }
  });
  

  app.post('/login', (req, res) => {
    const { email, password } = req.body;
    if (!email || !password) {
      res
        .status(400)
        .json({ errorMessage: 'Please specify both email and password' });
    } else {
      // Vérification de l'existence d'un compte avec cet email
      connection.query(
        `SELECT * FROM user WHERE email=?`,
        [email],
        (mysqlError, result) => {
          if (mysqlError) {
            res.status(500).json({ error: mysqlError });
          } else if (result.length === 0) {
            res.status(401).json({ error: 'Invalid email' });
          } else {
            const user = result[0];
            // Récupérer le mot de passe haché en base de données.
            const hashedPassword = user.password;
            // Comparer avec bcrypt le mot de passe haché et le mot de passe fourni en clair.
            bcrypt.compare(password, hashedPassword, function(bcryptError, passwordMatch) {
              if (bcryptError) {
                res
                  .status(500)
                  .json({ error: bcryptError });
              } else if (passwordMatch) {
                // passwordMatch est vrai si le mot de passe correspond
                // Retourner le compte connecté sans le mot de passe
                res.status(200).json({
                  id: user.id,
                  email: user.email,
                });
              } else {
                res.status(401).json({ error: 'Invalid password' });
              }
            });
          }
        }
      );
    }
  });
  