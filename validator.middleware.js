const validateGarageData = (req, res, next) => {
    const { name, email } = req.body;
    if (!name || !email) {
        return res.status(400).json({ message: 'Les informations de nom et d\'email sont obligatoires.' });
    }

    const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      if (!emailRegex.test(email)) {
        return res.status(400).json({ message: 'Adresse e-mail invalide, veuillez entrer un format valide.' });
    }

next();

};

module.exports = { validateGarageData };
