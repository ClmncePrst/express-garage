const express = require('express');
const router = express.Router();
const connection = require("./conf/db");
const { validateGarageData } = require('./validator.middleware');




// Déplacer et adapter les routes vers /cars
router.get('/cars', (req, res) => {
    connection.query('SELECT * FROM car', (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la récupération des voitures');
        } else {
            // Envoi du résultat sous forme de JSON si la requête est réussie
            res.json(results);
        }
    });
});

router.get('/cars/:id', (req, res) => {
    const id = req.params.id;
    connection.execute('SELECT * FROM car WHERE car_id = ?', [id], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la récupération de la voiture');
        } else {
            res.json(results);
        }
    });
});

router.post('/cars', validateGarageData, (req, res) => {
    const { brand, model } = req.body;
    // Exécution d'une requête SQL pour insérer une nouvelle voiture
    connection.execute('INSERT INTO car (brand, model) VALUES (?, ?)', [brand, model], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de l\'ajout de la voiture');
        } else {
            // Confirmation de l'ajout de la voiture avec l'ID de la voiture inséré
            res.status(201).send(`Voiture ajoutée avec l'ID ${results.insertId}`);
        }
    });
});

router.put('/cars/:id', validateGarageData, (req, res) => {
    const { brand, model } = req.body;
    const id = req.params.id;
    // Exécution d'une requête SQL pour mettre à jour les informations de la voiture choisie
    connection.execute('UPDATE car SET brand = ?, model = ? WHERE car_id = ?', [brand, model, id], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la mise à jour de la voiture');
        } else {
            // Confirmation de la mise à jour de la voiture
            res.send(`Voiture mise à jour.`);
        }
    });
});

router.delete('/cars/:id', (req, res) => {
    const id = req.params.id;
    // Exécution d'une requête SQL pour supprimer la voiture choisie
    connection.execute('DELETE FROM car WHERE car_id = ?', [id], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la suppression de la voiture');
        } else {
            // Confirmation de la suppression de la voiture
            res.send(`Voiture supprimée.`);
        }
    });
});

module.exports = router;
